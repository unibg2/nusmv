package sampleperspectiv.views;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.Platform;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class RunNuSMV 
{
	
	
	public static ArrayList RunNuSMV() throws IOException, ParserConfigurationException, SAXException 
	{
		
		ArrayList<CounterExample> var = new ArrayList<CounterExample>();		
		
		ArrayList <ArrayList<CounterExample>> allVar = new ArrayList <ArrayList<CounterExample>>();
		
		ProcessBuilder pb = new ProcessBuilder("NuSMV.exe","-int");
		Process process = pb.start();
				
		
		OutputStream out = process.getOutputStream();
		// Write commands
		PrintWriter commands = new PrintWriter( out , true );	
		
		commands.println("reset");
		commands.println("set default_trace_plugin 4");				
		commands.println("read_model -i C:\\temp/ascensore.smv");				
		commands.println("go");
		
		
		String path = (""+Platform.getInstanceLocation().getURL()).replace("file:/","");
		path = path+"/RemoteSystemsTempFiles";
		
		
		FileReader f;
		f = new FileReader(path+"/false.txt");
		BufferedReader b;
		b = new BufferedReader(f);
		
		String s1;
		String s2;
		ArrayList<String> prop = new ArrayList<String>();
		ArrayList<String> propType = new ArrayList<String>();		
		
		while(true) 
		{
			s1 = b.readLine();
			if( s1==null )
			{  
				break;
			}
			else
			{
				if(s1.indexOf(": ")>0)
				{ // STO ANALIZZANDO UNA RIGA CHE CONTIENE LA PROPRIETA
					
					s1 = s1.substring(s1.indexOf(": ")+2);					
					prop.add(s1);
					
					//LEGGO LA RIGA SUCCESSIVA: CONTIENE CTL/ LTL					
					s2 = b.readLine();
					if(s2.indexOf("LTL")>0)
					{
						propType.add("ltlspec");					
					}
					else if(s2.indexOf("CTL")>0)
					{
						propType.add("ctlspec");
						
					}else 
					{
						propType.add("invar");						
						// in questo branch controlleṛ gli invar
					}
				}				
			}	      
	    }
		
		String proprieta;
		String tipoProrieta;
		
		for (int k = 0; k < prop.size(); k++) 
		{			
			proprieta = prop.get(k);
			tipoProrieta = propType.get(k);
			commands.println( "check_"+tipoProrieta+" -p '"+proprieta+"'" ); 
			commands.println( "show_traces -o "+path+"/showtraces-"+k+".xml" );	
			
			FileWriter w;
			w = new FileWriter(path+"/proprieta-"+k+".txt");
		    BufferedWriter buffer = new BufferedWriter (w);
		    buffer.write(tipoProrieta+ ": "+proprieta);
		    buffer.flush();
		}
		commands.flush();
		commands.close();
		try {
			out.flush();
		} catch (IOException e1) {			
			e1.printStackTrace();
		}
		try {
			out.close();
		} catch (IOException e) {			
			e.printStackTrace();
		}
		
		for(int j=0;j< prop.size(); j++)
		{
			/* XML */
			File fXmlFile = new File(path+"/showtraces-"+j+".xml");		
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
	
			doc.getDocumentElement().normalize();
	
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
					
			NodeList nList = doc.getElementsByTagName("state");
			
			for (int temp = 0; temp < nList.getLength(); temp++) 
			{
	
				Node nNode = nList.item(temp);
						
				if (nNode.getNodeType() == Node.ELEMENT_NODE) 
				{
	
					Element eElement = (Element) nNode;
	
					System.out.println("State id : " + eElement.getAttribute("id"));
					String state =  eElement.getAttribute("id");
					String[] vargs = new String[eElement.getElementsByTagName("value").getLength()+1];
					String[] titles = new String[eElement.getElementsByTagName("value").getLength()+1];				
					for(int i = 1; i <= eElement.getElementsByTagName("value").getLength() ; i++ ) 
					{
						System.out.println(eElement.getElementsByTagName("value").item(i-1).getAttributes().getNamedItem("variable").getNodeValue() + " : " + eElement.getElementsByTagName("value").item(i-1).getTextContent());
						if(temp==0)
						{
							if(i==1)
							{
								titles[i-1] ="State";
								titles[i] = eElement.getElementsByTagName("value").item(i-1).getAttributes().getNamedItem("variable").getNodeValue();
							}
							else
							{
								titles[i] = eElement.getElementsByTagName("value").item(i-1).getAttributes().getNamedItem("variable").getNodeValue();
							}
						}
						
						if(i==1)
						{
							vargs[i-1]= state;
							vargs[i] = eElement.getElementsByTagName("value").item(i-1).getTextContent();    
						}
						else
						{
							vargs[i] = eElement.getElementsByTagName("value").item(i-1).getTextContent();    
						}
					}
					if(temp==0)
					{
						var.add(new CounterExample(titles ));
					}
					var.add(new CounterExample(vargs ));
				}
				
			}
			allVar.add(j, var);
			var = new ArrayList<CounterExample>();
			
		} 
		/* XML */
	
		return allVar;
	} 
	
	
	public static ArrayList getProp() throws IOException, ParserConfigurationException, SAXException 
	{
		
		ArrayList<String> sProperty = new ArrayList<String>();		
		
		String path = (""+Platform.getInstanceLocation().getURL()).replace("file:/","");
		path = path+"/RemoteSystemsTempFiles";
		
		
		FileReader f;
		f = new FileReader(path+"/false.txt");
		BufferedReader b;
		b = new BufferedReader(f);
		String s1;
		String s2;
		while(true) 
		{
			s1 = b.readLine();
			if( s1==null )
			{  
				break;
			}
			else
			{
				if(s1.indexOf(": ")>0)
				{ // STO ANALIZZANDO UNA RIGA CHE CONTIENE LA PROPRIETA
					
					s1 = s1.substring(s1.indexOf(": ")+2);	
					
					//LEGGO LA RIGA SUCCESSIVA: CONTIENE CTL/ LTL					
					s2 = b.readLine();
					if(s2.indexOf("LTL")>0)
					{						
						sProperty.add("LTL: "+s1);
					}
					else if(s2.indexOf("CTL")>0)
					{						
						sProperty.add("CTL: "+s1);
					}
					else 
					{
						sProperty.add("INVAR: "+s1);						
					}
				}				
			}	      
	    }
		
		return sProperty;
	} 
	


}

