package sampleperspectiv.views;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.xml.sax.SAXException;

public class CustomAction extends Action implements IWorkbenchAction
{

private static final String ID = "com.timmolter.helloWorld.CustomAction";

private int index;
TableViewer viewer;
SampleView view;
String title;
public CustomAction(int index, TableViewer viewer, SampleView view,String title)
{
	setId(ID);
	this.index = index;
	this.viewer = viewer;	
	this.view = view;
	this.title = title;
}

public void run() 
{
	
	viewer.setInput(ModelProvider.INSTANCE.getList(index));
	view.cambiaNome(title);
	/*Shell shell1 = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
	String dialogBoxTitle1 = "Message";
	String message1 = "You clicked index!"+this.index;
	MessageDialog.openInformation(shell1, dialogBoxTitle1, message1);*/
	
	
}


public void dispose() {}


	

}



