package sampleperspectiv.views;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public enum ModelProvider {
  INSTANCE;

  private List<Person> persons;
  ArrayList<ArrayList<CounterExample>> listAll;
  
  private ModelProvider() {
    try {
		listAll = RunNuSMV.RunNuSMV();
	} catch (IOException e) {
		e.printStackTrace();
	} catch (ParserConfigurationException e) {
		e.printStackTrace();
	} catch (SAXException e) {
		e.printStackTrace();
	}
    
    
  }

 /* public List<Person> getPersons() {
    return persons;
  }*/
  
  public List<CounterExample> getList(int index) {
	  return listAll.get(index);
  }

} 
