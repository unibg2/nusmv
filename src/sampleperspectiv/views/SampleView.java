package sampleperspectiv.views;



import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jws.Oneway;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.*;
import org.eclipse.jface.viewers.*;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.jface.action.*;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.*;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.SWT;
import org.xml.sax.SAXException;

import sampleperspectiv.Activator;


/**
 * This sample class demonstrates how to plug-in a new
 * workbench view. The view shows data obtained from the
 * model. The sample creates a dummy model on the fly,
 * but a real implementation would connect to the model
 * available either in this or another plug-in (e.g. the workspace).
 * The view is connected to the model using a content provider.
 * <p>
 * The view uses a label provider to define how model
 * objects should be presented in the view. Each
 * view can present the same model objects using
 * different labels and icons, if needed. Alternatively,
 * a single label provider can be shared between views
 * in order to ensure that objects of the same type are
 * presented in the same way everywhere.
 * <p>
 */

public class SampleView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	//public static final String ID = "sampleperspectiv.views.SampleView";
	public static final String ID = "viewID";
	protected static ArrayList<String> sProperty;
	private TableViewer viewer;
	private Action action1;
	private Action action2;
	private Action doubleClickAction;
	
	private PersonFilter filter;
	
	  private Text text;
	  private Table table;
	  private TableViewer tableViewer;
	  
	  public int index = 0;
	  
	  /*private static final Image CHECKED = Activator.getImageDescriptor("icons/checked.gif").createImage();
	  private static final Image UNCHECKED = Activator.getImageDescriptor("icons/unchecked.gif").createImage();*/


	/*
	 * The content provider class is responsible for
	 * providing objects to the view. It can wrap
	 * existing objects in adapters or simply return
	 * objects as-is. These objects may be sensitive
	 * to the current input of the view, or ignore
	 * it and always show the same content 
	 * (like Task List, for example).
	 */
	 
	  public static IViewPart showAdditionalViewPart(String viewID,String secondaryID){
		  IWorkbenchPage page=PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		  IViewPart viewPart=null;
		  try {
		    viewPart=page.showView(viewID,secondaryID,IWorkbenchPage.VIEW_ACTIVATE);
		  }
		 catch (  PartInitException ex) {
		    ex.printStackTrace();
		  }
		  return viewPart;
		}
	  
	  
	class ViewContentProvider implements IStructuredContentProvider {
		
		
		public void inputChanged(Viewer v, Object oldInput, Object newInput) {
		}
		public void dispose() {
		}
		public Object[] getElements(Object parent) {
			return new String[] { "One", "Two", "Three" };
		}
	}
	class ViewLabelProvider extends LabelProvider implements ITableLabelProvider {
		public String getColumnText(Object obj, int index) {
			return getText(obj);
		}
		public Image getColumnImage(Object obj, int index) {
			return getImage(obj);
		}
		public Image getImage(Object obj) {
			return PlatformUI.getWorkbench().
					getSharedImages().getImage(ISharedImages.IMG_OBJ_ELEMENT);
		}
	}
	class NameSorter extends ViewerSorter {
	}

	/**
	 * The constructor.
	 */
	public SampleView()
	{
		
	}


	public void createPartControl( Composite parent ) 
	{

	
		
		

		
		
		GridLayout layout = new GridLayout(2, false);
	    parent.setLayout(layout);
	    //Label searchLabel = new Label(parent, SWT.NONE);
	    //searchLabel.setText("Search: ");
	    /*final Text searchText = new Text(parent, SWT.BORDER | SWT.SEARCH);
	    searchText.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL
	        | GridData.HORIZONTAL_ALIGN_FILL));*/
	    
	    Thread t = new Thread( new RunSMV());
		t.start();	
	
		
		try
	    {	    	
			createViewer(parent);			
		} catch (IOException | ParserConfigurationException | SAXException e) {
			
			e.printStackTrace();
		}
		
		
		
				
	    
	    
	 // New to support the search
	    /*searchText.addKeyListener(new KeyAdapter() {
	      public void keyReleased(KeyEvent ke) {
	        filter.setSearchText(searchText.getText());
	        viewer.refresh();
	      }

	    });*/
	    //filter = new PersonFilter();
	   // viewer.addFilter(filter);
	    
	}
	
	
	public void createViewer(Composite parent) throws IOException, ParserConfigurationException, SAXException
	{
			
		
		/* START OUR STUFF */			
			
			ArrayList<ArrayList<CounterExample>> listAll = RunNuSMV.RunNuSMV();
			
			/*for( int z = 0; z < listAll.size() ; z++ )	
			{ */
				ArrayList<CounterExample> list = listAll.get(index);
				int numeroColonne = list.get(1).getN();
				
				/* END OUR STUFF*/
				
				viewer = new TableViewer( parent , SWT.MULTI | SWT.H_SCROLL
				        | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
				       
				// SCRIVO LE PROPRIETA NEL MENU
				ArrayList<String> prop = RunNuSMV.getProp();
				CustomAction menuAction[] = new CustomAction[prop.size()];
				for(int y = 0 ; y < prop.size() ; y++ )
				{					
					menuAction[y] = new CustomAction(y, viewer, this, prop.get(y));
					menuAction[y].setText(prop.get(y));
					menuAction[y].setImageDescriptor(Activator.getImageDescriptor("icons/sample.gif"));
					getViewSite().getActionBars().getMenuManager().add(menuAction[y]);
				
				}
				// SCRIVO LE PROPRIETA NEL MENU
				
		
				
			    createColumns(numeroColonne, parent , viewer);
			    final Table table = viewer.getTable();
			  
			    table.setLinesVisible(true);
			    viewer.setContentProvider(new ArrayContentProvider());
			    
			   
			    //viewer.setInput(ModelProvider.INSTANCE.getList(0));
			  			
			    getSite().setSelectionProvider(viewer);
			    GridData gridData = new GridData();
			    
			    gridData.verticalAlignment = GridData.FILL;			    
			    gridData.horizontalSpan = 2;
			    gridData.grabExcessHorizontalSpace = true;
			    gridData.grabExcessVerticalSpace = true;
			    gridData.horizontalAlignment = GridData.FILL;
			    
			    viewer.getControl().setLayoutData(gridData);
			/*}*/
			
		    /*String value[] = new String[numeroColonne];
		    String valuePrev[] = new String[numeroColonne];
		    
		    TableItem [] items = table.getItems();		    
		    Display display = Display.getCurrent();
		    */
	    	/*for(int i = 0; i < items.length; i++ ) 
	    	{ 
		    	for(int j = 0; j < numeroColonne; j++ )
		    	{
			    	TableItem item = items[i];
			    	
			    	value[j] = item.getText(j);			    	
			    	if(j != 0 && i >= 2 && value[j].equals(valuePrev[j]) == false )
			    	{
			    		display = Display.getCurrent();
			    		Color yellow = display.getSystemColor(SWT.COLOR_YELLOW);
			    		item.setForeground(yellow);
			    		
			    		Color green = display.getSystemColor(SWT.COLOR_GREEN);
			    		//item.setBackground(green);
			    		item.setForeground(j, green);
			    	}			    	
			    	valuePrev[j] = value[j];			    	
			    }
	    	}*/	    	 
	}
	
	 public TableViewer getViewer() 
	 {
		    
		 return viewer;
	 }
	 
	 public void cambiaNome(String title) 
	 {
				 setPartName(title);
	 }

	// create the columns for the table
	 private void createColumns(int numeroColonne,final Composite parent, final TableViewer viewer) 
	 {
		 // ciclo creazione colonne
		 			
		    		TableViewerColumn col;
				 	for(int i=0;i<numeroColonne;i++)
				 	{
				 		int j=i;
				 		col = createTableViewerColumn("", 100, j);
				    	col.setLabelProvider(new ColumnLabelProvider() 
				    	{
						      @Override
						      public String getText(Object element) 
						      {
						        CounterExample p = (CounterExample) element;
						        return p.getVar(j);		    	 
						      }
						});
				 	}
		 		// ciclo creazione colonne
		    
	 }
	 
	 // create the columns for the table
	  /*private void createColumns(final Composite parent, final TableViewer viewer) {
	    String[] titles = { "First name", "Last name", "Gender", "Married","Gender", "Married" };
	    int[] bounds = { 100, 100, 100, 100, 100, 100 };

	    // first column is for the first name
	    TableViewerColumn col = createTableViewerColumn(titles[0], bounds[0], 0);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	        CounterExample p = (CounterExample) element;
	        return p.getVar(0);
	      }
	    });

	    // second column is for the last name
	    col = createTableViewerColumn(titles[1], bounds[1], 1);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  CounterExample p = (CounterExample) element;
		        return p.getVar(1);
	      }
	    });

	    // now the gender
	    col = createTableViewerColumn(titles[2], bounds[2], 2);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  CounterExample p = (CounterExample) element;
		        return p.getVar(2);
	      }
	    });
	    

	    // now the gender
	    col = createTableViewerColumn(titles[2], bounds[3], 3);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  CounterExample p = (CounterExample) element;
		        return p.getVar(3);
	      }
	    });
	    

	    // now the gender
	    col = createTableViewerColumn(titles[4], bounds[4], 4 );
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	    	  CounterExample p = (CounterExample) element;
		        return p.getVar(4);
	      }
	    });

	    // now the status married
	    /*col = createTableViewerColumn(titles[3], bounds[3], 3);
	    col.setLabelProvider(new ColumnLabelProvider() {
	      @Override
	      public String getText(Object element) {
	        return null;
	      }

	      @Override
	      public Image getImage(Object element) {
	        if (((Person) element).isMarried()) {
	          return CHECKED;
	        } else {
	          return UNCHECKED;
	        }
	      }
	    });*/

	  /*}*/
	 
	 class PersonFilter extends ViewerFilter{

		  private String searchString;

		  public void setSearchText(String s) {
		    // ensure that the value can be used for matching 
		    this.searchString = ".*" + s + ".*";
		  }

		  @Override
		  public boolean select(Viewer viewer, 
		      Object parentElement, 
		      Object element) {
		    if (searchString == null || searchString.length() == 0) {
		      return true;
		    }
		    CounterExample p = (CounterExample) element;
		    if (p.getVar(0).matches(searchString)) {
			      return true;
			}	
		    if (p.getVar(1).matches(searchString)) {
		      return true;
		    }	
		    if (p.getVar(2).matches(searchString)) {
			      return true;
			}
		    if (p.getVar(3).matches(searchString)) {
			      return true;
			}
		    return false;
		  }
		} 
	

		  private TableViewerColumn createTableViewerColumn(String title, int bound, final int colNumber) 
		  {
		    final TableViewerColumn viewerColumn = new TableViewerColumn(viewer, SWT.NONE);
		    final TableColumn column = viewerColumn.getColumn();
		    column.setText(title);
		    column.setWidth(bound);
		    column.setResizable(true);
		    column.setMoveable(true);
		    return viewerColumn;
		  }

		

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}
}