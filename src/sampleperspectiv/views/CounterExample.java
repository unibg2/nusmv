package sampleperspectiv.views;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;

public class CounterExample {
  private String[] var;
  private String state;
  private int n;
  private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

  public CounterExample(){}
  
  public CounterExample(String... vargs)
  {
     super();     
     this.n = vargs.length;
     var = new String[n];
     for( int i = 0; i < n ; i++)
     {
    	this.var[i] = vargs[i];
     }
  }
  
  public String getVar(int j)
  {
	  return var[j];
  }
  
  public int getN()
  {
	  return n;
  }
  
  public String[] getRow()
  {
	  return var;
  }

  public void addPropertyChangeListener(String propertyName,
      PropertyChangeListener listener) {
    propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
  }

  public void removePropertyChangeListener(PropertyChangeListener listener) {
    propertyChangeSupport.removePropertyChangeListener(listener);
  }
  @Override
  public String toString() {
	 String ret=" / ";
	  for( int i = 0; i < var.length ; i++)
	     {
	    	ret+=var[i]+=" / ";
	     }
	  return ret;
  }
} 