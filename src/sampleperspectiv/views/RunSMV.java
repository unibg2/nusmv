package sampleperspectiv.views;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.Bundle;

public class RunSMV implements Runnable
{
		public RunSMV()
		{
			
		}
		
		@Override
		public void run() 
		{
			ProcessBuilder pb = new ProcessBuilder("NuSMV.exe","-int");
			Process process = null;
			try {
				process = pb.start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		
				
			String path = (""+Platform.getInstanceLocation().getURL()).replace("file:/","");
			path = path+"/RemoteSystemsTempFiles";
			
			System.out.println(path);
			OutputStream out = process.getOutputStream();
			// Write commands
			PrintWriter commands = new PrintWriter(out,true);		
			commands.println("reset");
			commands.println("set default_trace_plugin 4");				
			commands.println("read_model -i C:\\temp/ascensore.smv");				
			commands.println("go");
			commands.println("check_ctlspec");
			commands.println("check_ltlspec");
			commands.println("check_invar");			
			commands.println("show_property -f -o "+path+"/false.txt");
			commands.flush();
			commands.close();
			try {
				out.flush();
			} catch (IOException e1) {				
				e1.printStackTrace();
			}
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}	
}
